# Author - Mark Jerome Fonacier Reyta	


# To test run command below

1. composer install 
2. php artisan migrate
3. php artisan db:seed
4. php artisan serve

# deployed in heroku due to having troubles in cors but was able to make it work in localhosts
http://stormy-tundra-14075.herokuapp.com/
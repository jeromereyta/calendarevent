<?php

use Illuminate\Database\Seeder;

class EventSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert([
            'name'        => 'Final Interview',
            'description' => 'This will be the final judgment day',
            'days'        => 'monday',
            'start_date'  => '2020-03-02',
            'end_date'    => '2020-03-02',
            'created_at'  => '2020-03-01',
            'updated_at'  => '2020-03-01',
            'deleted_at'  => null,
        ]);

        DB::table('event_schedules')->insert([
            'event_id'   => '1',
            'day'        => 'monday',
            'date'       => '2020-03-02',
            'created_at' => '2020-03-01',
            'updated_at' => '2020-03-01',
            'deleted_at' => null,
        ]);

        DB::table('events')->insert([
            'name'        => 'Job Offer',
            'description' => 'Negotiation and Signing of the contract',
            'days'        => 'thursday',
            'start_date'  => '2020-03-05',
            'end_date'    => '2020-03-05',
            'created_at'  => '2020-03-01',
            'updated_at'  => '2020-03-01',
            'deleted_at'  => null,
        ]);

        DB::table('event_schedules')->insert([
            'event_id'   => '2',
            'day'        => 'thursday',
            'date'       => '2020-03-05',
            'created_at' => '2020-03-01',
            'updated_at' => '2020-03-01',
            'deleted_at' => null,
        ]);

    }
}

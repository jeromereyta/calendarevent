<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace'=>'API','middleware' => ['api', 'cors']], function()
{
    foreach ( File::allFiles(__DIR__.'/API') as $partial )
    {
        require_once $partial->getPathName();
    }
});
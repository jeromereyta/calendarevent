<?php

Route::group(['middleware' => ['cors']], function () {
    Route::resource('event', 'EventController', ['except' => ['show']]);
});

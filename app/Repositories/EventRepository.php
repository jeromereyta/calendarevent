<?php

namespace App\Repositories;

use App\Models\Event;

class EventRepository extends BaseRepository
{
    protected $model;

    public function __construct(Event $event)
    {
        $this->model = $event;
    }
}

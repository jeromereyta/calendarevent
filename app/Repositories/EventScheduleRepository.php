<?php

namespace App\Repositories;

use App\Models\EventSchedule;
use Illuminate\Support\Facades\DB;

class EventScheduleRepository extends BaseRepository
{
    protected $model;

    public function __construct(EventSchedule $eventSchedule)
    {
        $this->model = $eventSchedule;
    }

    public function createEventSchedules(array $data = [])
    {
        return DB::table('event_schedules')->insert($data);
    }

    public function deleteEventSchedulesByEventID(int $event_id)
    {
        return DB::table('event_schedules')->where('event_id', $event_id)->delete();
    }

    public function fetchSchedulesEvents()
    {
        $results = [];

        $data = $this->model->with('event')->get();

        foreach ($data as $key => $value) {
            $results[$key]['event_id'] = $value['event']['id'];
            $results[$key]['title']    = $value['event']['name'];
            $results[$key]['details']  = $value['event']['description'];
            $results[$key]['days']     = explode(',', $value['event']['days']);
            $results[$key]['date']     = $value['date'];
            $results[$key]['bgcolor']  = 'blue';
            $results[$key]['endDate']  = $value['event']['end_date'];

        }

        return $results;
    }
}

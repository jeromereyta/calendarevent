<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Api\ApiController;
use App\Services\EventService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EventController extends ApiController
{
    protected $eventService;

    public function __construct(EventService $eventService)
    {
        parent::__construct();

        $this->eventService = $eventService;
    }

    protected function jsonResponse($data, $code = 200)
    {
        return $this->response->json(['data' => $data, 'code' => $code]);
    }

    public function index()
    {
        return $this->jsonResponse($this->eventService->getAllScheduleEvents(), 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'       => 'required|unique:events|max:255',
            'start_date' => 'date',
            'end_date'   => 'date',
        ]);

        if ($validator->fails()) {
            return $this->jsonResponse($validator->errors(), 400);
        } else {
            return $this->jsonResponse($this->eventService->createEvent($request->all()), 200);
        }

    }

    public function update($event_id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'start_date' => 'date',
            'end_date'   => 'date',
        ]);

        if ($validator->fails()) {
            return $this->jsonResponse($validator->errors(), 400);
        } else {
            return $this->jsonResponse($this->eventService->updateEvent($event_id, $request->all()), 200);
        }
    }

}

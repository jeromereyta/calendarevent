<?php

namespace App;
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventSchedule extends Model
{
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['event_id','day','date'];

    /**
     * Get the post that owns the comment.
     */
    public function event()
    {
        return $this->belongsTo('App\Models\Event');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
	use SoftDeletes;

   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','description','days','start_date','end_date'];
	
    public function schedules() {
    	return $this->hasMany('App\Models\EventSchedule');
    }


    public function createEvent($event = []) {
		$flight = App\Flight::firstOrCreate(['name' => 'Flight 10']);
    }

}

<?php

namespace App\Services;

use App\Repositories\EventRepository;
use App\Repositories\EventScheduleRepository;

class EventService
{
    private $eventRepository;
    private $scheduleRepository;

    public function __construct(EventRepository $eventRepository, EventScheduleRepository $scheduleRepository)
    {
        $this->eventRepository    = $eventRepository;
        $this->scheduleRepository = $scheduleRepository;

    }

    public function getAllScheduleEvents()
    {
        return $this->scheduleRepository->fetchSchedulesEvents();
    }

    public function createEvent(array $data = [])
    {
        $data['days'] = implode(',', $data['days']);        
        $newEvent = $this->eventRepository->create($data);
        $data['days'] = explode(',', $data['days']);        

        return $this->createScheduleDates(
            $newEvent['id'],
            $data['start_date'],
            $data['end_date'],
            $data['days'],
            'Y-m-d'
        );
    }

    /* Source code:
     *  https://www.codementor.io/tips/1170438972/how-to-get-an-array-of-all-dates-between-two-dates-in-php
     *  modified to include filters by days of week
     */
    public function createScheduleDates(
        int $event_id,
        string $startDate,
        string $endDate,
        array $days = [],
        string $format = "Y-m-d"
    ) {
        $dateRangeArray = \Carbon\CarbonPeriod::create($startDate, $endDate)->toArray();

        $alldates = [];

        foreach ($dateRangeArray as $key => $carbonObj) {

            if (!empty($days) && in_array(strtolower($carbonObj->englishDayOfWeek), $days)) {
                $alldates[$key]['event_id'] = $event_id;
                $alldates[$key]['date']     = $carbonObj->format($format);
                $alldates[$key]['day']      = $carbonObj->englishDayOfWeek;
            }

            if (empty($days)) {
                $alldates[$key]['event_id'] = $event_id;
                $alldates[$key]['date']     = $carbonObj->format($format);
                $alldates[$key]['day']      = $carbonObj->englishDayOfWeek;
            }
        }

        $this->scheduleRepository->createEventSchedules($alldates);

        return $this->scheduleRepository->fetchSchedulesEvents();
    }

    public function updateEvent(int $event_id, array $data = [])
    {
        $this->eventRepository->update($event_id, [
            'name'        => $data['name'],
            'description' => $data['description'],
            'days'        => implode(',',$data['days']),
            'start_date'  => $data['start_date'],
            'end_date'    => $data['end_date']
        ]);

        $this->scheduleRepository->deleteEventSchedulesByEventID($event_id);

        return $this->createScheduleDates(
            $event_id,
            $data['start_date'],
            $data['end_date'],
            $data['days'],
            'Y-m-d'
        );
    }
}
